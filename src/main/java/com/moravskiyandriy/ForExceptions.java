package com.moravskiyandriy;

import com.moravskiyandriy.deliberateexception.CustomDeliberateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class ForExceptions implements AutoCloseable {
    private static final Logger Logger = LogManager.getLogger(ForExceptions.class);
    ForExceptions() {
    }

    @Override
    public void close() throws IOException, CustomDeliberateException {
        Logger.info("Closing resource.");
        throw new CustomDeliberateException("Throwing an exception");
    }
}
