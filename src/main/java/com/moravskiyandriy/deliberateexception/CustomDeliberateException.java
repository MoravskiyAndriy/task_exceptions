package com.moravskiyandriy.deliberateexception;

public class CustomDeliberateException extends Exception {
    public CustomDeliberateException(String errorMessage){
        super(errorMessage);
    }
}