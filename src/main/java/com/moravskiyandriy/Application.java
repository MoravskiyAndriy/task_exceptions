package com.moravskiyandriy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Application {
    private static final Logger Logger = LogManager.getLogger(Application.class);

    static void displayExceptions(String data) {
        try (ForExceptions newException = new ForExceptions()){
            Logger.info("Accessing data: " + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        displayExceptions("An old archive.");
    }
}

